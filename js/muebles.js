 // Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove} from "https://www.gstatic.com/firebasejs/9.17.2/firebase-database.js";
import { getStorage,ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyCMH7xUSqUf_jmBMJyEr2V6-F5StHUF05Q",
    authDomain: "proyectocorte3-29028.firebaseapp.com",
    databaseURL: "https://proyectocorte3-29028-default-rtdb.firebaseio.com",
    projectId: "proyectocorte3-29028",
    storageBucket: "proyectocorte3-29028.appspot.com",
    messagingSenderId: "631621409783",
    appId: "1:631621409783:web:13a108fd9dd721bbc7acf2"
  };

  // Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

// Generar Productos
let productos = document.getElementById('contenido-productos');
window.addEventListener('DOMContentLoaded',mostrarProductos);

function mostrarProductos(){
    const dbRef = ref(db, "productos");


    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="1"){

            productos.innerHTML +=`
                <div class='producto'>
                <img class='img-item' src='${childData.urlImagen}'>
                <p class='nombre'>${childData.nombre}</p>
                <p class='descripcion' style='font-size: .9em;'>${childData.descripcion}</p>
                <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                <p class='precio'>\$${childData.precio}</p>
                <button class='boton-comprar' data-bs-toggle="modal" data-bs-target="#exampleModalToggle">Comprar</button>
                </div>
            `;
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}
